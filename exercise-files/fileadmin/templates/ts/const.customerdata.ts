baseurl=http://p206346.mittwaldserver.info

sys_language_uid = 0
language = de
locale_all = de_DE
htmltag_langkey = de

meta.author = AC
meta.publisher = brandnamic | hotel & destination marketing
meta.copyright = , brandnamic hotel & destination marketing
meta.robots = index, follow

[globalString= IENV:HTTP_HOST = *mittwaldserver.info] OR [globalString= IENV:HTTP_HOST = *plusserver.de] OR [globalString= IENV:HTTP_HOST = *brandnamic.com] OR [globalString= IENV:HTTP_HOST = *mobile.*]
meta.robots = noindex, nofollow
[global]

[globalVar=GP:L=1]
sys_language_uid = 1
language = it
locale_all = it_IT
htmltag_langkey = it
[global]

[globalVar=GP:L=2]
sys_language_uid = 2
language = en
locale_all = en_EN
htmltag_langkey = en
[global]

[globalVar=GP:L=3]
sys_language_uid = 3
language = fr
locale_all = fr_FR
htmltag_langkey = fr
[global]

[globalVar=GP:L=4]
sys_language_uid = 4
language = nl
locale_all = nl_NL
htmltag_langkey = nl
[global]