page = PAGE
page {
	typeNum = 0
	config {
		admPanel = 0
		baseURL = {$baseurl}/
		pageTitleFirst = 1
		doctype = html5
		xmlprologue = none
		metaCharset = UTF-8
		locale_all = {$locale_all}
		linkVars = L(0-4)
		uniqueLinkVars = 1
		sys_language_uid = {$sys_language_uid}
		language = {$language}
		htmlTag_setParams = class="no-js" lang="{$htmltag_langkey}"
		sys_language_mode = content_fallback
		sys_language_overlay = 1
		meaningfulTempFilePrefix = 100
		noPageTitle = 2 
		removeDefaultJS = external
		inlineStyle2TempFile = 1
		disablePrefixComment = false
		noBlur = 1
		spamProtectEmailAddresses = ascii
		spamProtectEmailAddresses = -3
		spamProtectEmailAddresses_atSubst = @<span class="hidden">remove-this.</span>
		simulateStaticDocuments = 0
		prefixLocalAnchors = all
		tx_cooluri_enable = 1
		redirectOldLinksToNew = 1
		intTarget = _top
		extTarget = _blank
		no_cache = 0
	}
	
	meta {
		description = {$meta.description}
		author = {$meta.author}
		publisher = {$meta.publisher}
		robots = {$meta.robots}
	}
	
	headerData {
		1 = TEXT
		1.field = subtitle // title
		1.wrap = <title>|</title>
	}
}

page.bodyTagCObject = HTML
page.bodyTagCObject.value.field = uid
page.bodyTagCObject.value.noTrimWrap = |<body id="uid|" class="{$language}" data-lang="{$language}">|

[globalVar = TSFE:id = 9999]
config.additionalHeaders = HTTP/1.1 404 Not Found
[global]

[browser = msie] AND [version= <7]
	page.config.doctypeSwitch = 1
[global]

[globalVar = TSFE : beUserLogin = 1]
	page.config.no_cache = 1
[global]

page.includeCSS {
  style=fileadmin/templates/css/main.css
  style.media=screen
  print=fileadmin/templates/css/print.css
  print.media=print
}

page.includeJSlibs {
	jquery=fileadmin/templates/js/vendor/jquery-1.9.1.js
	modernizr=fileadmin/templates/js/vendor/modernizr.2.6.2.js
	#	caro=fileadmin/templates/js/vendor/jquery.carouFredSel-6.2.0-packed.js
}

page.includeJS {
	inline=fileadmin/templates/js/scripts.js
}