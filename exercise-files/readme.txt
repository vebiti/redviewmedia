Hallo Thomas,

anbei findest du Screendesign als *.psd und marginale Ordnerstruktur des Webs, wie sie sp�ter in unseren Projekten auch online Verwendung findet.

Bitte k�mmere dich um folgende Dinge:

- Pixelgenaue Umsetzung des Templates laut Screendesign in HTML5/CSS3
- Validierung HTML durch W3C
- Validierung CSS bei Verwendung von CSS3 nicht zwingend erforderlich
- Crossbrowser-Kompatibilit�t (Chrome/Firefox/Safari/IE7-11)
- Umsetzung von ben�tigten JS-Animationen und Funktionalit�ten mit Hilfe von jquery (Klappbox / Slider / Men�f�hrung)

F�r eventuelle Fragen und Infos zur Umsetzung bin ich nat�rlich jederzeit erreichbar: alex.complojer@brandnamic.com

Gr��e
Alex

=======================================

N�tzliche Plugins f�r Firefox:

- Firebug
- Webdeveloper Toolbar

=======================================

Weiterf�hrende Themen:

- JSON-Webservices / Ajax
- Extensionentwicklung mit Extbase und Fluid
- Grundlagen OOP/MVC
- Symfony2
- Ext-JS


