/*jslint browser: true, devel: true, white: true*/
/*global $*/

(function() {
    'use strict';

    // inital remove of expanded classes, so that the site also works with javascript disabled

    var expandable = $('.expandable'),
        slider = {},
        networksCarousel,
        menu = $('#menu'),
        $document = $(document);



    // expandable elements

    $('.expanded').removeClass('expanded');

    $('.button-expand').on('click', function(element) {

        element.preventDefault();

        var $this = $(this),
            id = $this.attr('data-id-to-expand'),
            expandable = $('#' + id),
            expandScrollHeight = expandable[0].scrollHeight + 'px',
            expandHeight = expandable.css('height');

        if (expandHeight === expandScrollHeight) {
            expandable
                .css('height', '')
                .removeClass('expanded');

            $this.removeClass('button-expanded-state');

        } else {
            expandable
                .css('height', expandScrollHeight)
                .addClass('expanded');

            $this.addClass('button-expanded-state');
        }

    });


    // menu button

    $document.on('click', function() {
        menu.removeClass('show');
    });

    menu.on('click', 'a', function(e) {
        menu.removeClass('show');
    });

    $('#menuButton').on('click', function(event) {
        menu.toggleClass('show');
        event.stopPropagation();
    });

    // smooth scrolling

    //$('a[href*=#]:not([href=#])').on('click', function(element) {
    $document.on('click', 'a[href*=#]:not([href=#])', function(element) {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                element.preventDefault();
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 500);
            }
        }
    });


    // owl-carousel

    networksCarousel = $('.owl-carousel');

    networksCarousel.owlCarousel({
        itemsCustom : [
            [0, 1],
            [851, 2],
            [1311, 3]
        ]
    });

    $('.carousel-next').click(function(){
        networksCarousel.trigger('owl.next');
    });

    $('.carousel-prev').click(function(){
        networksCarousel.trigger('owl.prev');
    });

}());
